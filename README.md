File-system-manager

This application allows the user to gather various information about files contained within the resource folder embedded in the project. 
For one of the files, dracula.txt, the user can search for a word and see if and how many times the word occures in the file.

This project was made as a submission to a assignment for Experis Academy. 
