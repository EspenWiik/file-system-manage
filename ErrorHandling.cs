﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment1
{
    public class ErrorHandling
    {
        public static void CheckInput(string input, Action method)
        {
            try
            {
                int.Parse(input);
            }
            catch
            {
                Console.WriteLine($"'{input}' is invalid input, please try again");
                method();
            }
        }
    }
}
