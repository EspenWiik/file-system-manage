﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Assignment1
{
    public class Logging
    {
        public static void WriteToFile (string item, long time)
        {
            StreamWriter streamWriter = new StreamWriter(@"logging/Log.txt", true);
            //Console.WriteLine($"{DateTime.Now}: {item}. This process tok {time} ms to execute");
            streamWriter.WriteLine("<----------New Process--------->");
            streamWriter.WriteLine($"{DateTime.Now}: {item}\nThis process tok {time} ms to execute");
            streamWriter.Close();

        }
    }
}
