﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Assignment1
{
    public class Program
    {
        static void Main(string[] args)
        {
            UserInput();
        }

        public static void UserInput ()
        {
            // Give user instructions, gather input and run appropriate method
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<---------------GREETINGS--------------->");
            sb.AppendLine("Welcome to this application");
            sb.AppendLine("Please select one of the following options");
            sb.AppendLine("1. FileNames");
            sb.AppendLine("2. File Extension");
            sb.AppendLine("3. Dracula.txt options");
            sb.AppendLine("4. Exit application");

            Console.WriteLine(sb);
            string option = Console.ReadLine();

            //Error handling
            ErrorHandling.CheckInput(option, UserInput);

            if (option == "1")
            {
                FileNames();
            }
            else if (option == "2")
                FileNameByType();
            else if (option == "3")
                Dracula.PromptDracula();
            else if (option == "4")
            {
                Environment.Exit(0);
            }



        }

        // Method for displaying all filenames in folder
        static void FileNames ()
        {
            string path = @"resources";
            List<string> files = new List<string>();

            foreach (string file in Directory.GetFiles(path))
            {
                Console.WriteLine(file);
                files.Add(file);
            }
            UserInput();
        }

        // Method to allow user to see all files with a certain extension
        static void FileNameByType ()
        {
            string path = @"resources";

            Console.WriteLine("Please select the file extension you would like");
            Console.WriteLine("You can choose from: ");

            HashSet<string> extensions = new HashSet<string>();
            
            //Get and print all unique extensions from files in folder
            foreach (string file in Directory.GetFiles(path)) 
            {
                string ext = file.Substring(file.LastIndexOf('.') + 1);
                extensions.Add(ext);
            }

            foreach ( string ext in extensions)
            {
                Console.WriteLine($"- {ext}");
            }

            // Prompt user for which input they wish to search for and 
            Console.WriteLine("Please type in the extension you are looking for:... (eg. 'txt'");
            string opt = "*." + Console.ReadLine();

            List<string> list = new List<string>();
            foreach (string file in Directory.GetFiles(path, opt))
                list.Add(file);
           
            //Check if file exist, else re-execute prompt
            if (list.Count > 0)
                foreach (string file in list)
                    Console.WriteLine(file);
            else
            {
                Console.WriteLine($"There are no filed that have the extension {opt}, try again.");
                FileNameByType();
            }
                

            UserInput();
        }
    }
}
