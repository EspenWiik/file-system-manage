﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Assignment1
{
    public class Dracula
    {
        public static void PromptDracula()
        {
            // Provide user instructions and display generic file info
            string option = "0";
            var fileInfo = FileInfo();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<---------------DRACULA--------------->");
            sb.AppendLine("Please select one of the following options:");
            sb.AppendLine($" - File Name: {fileInfo.Item1}");
            sb.AppendLine($" - File Size: {fileInfo.Item2}");
            sb.AppendLine($" - Number of Lines: {fileInfo.Item3}");
            sb.AppendLine("1. Check if word exist and number of times it occures");
            sb.AppendLine("2. Go Back");

            Console.WriteLine(sb);

            // User input gatherd and run appropriate method
            option = Console.ReadLine();
            ErrorHandling.CheckInput(option, PromptDracula);
            
            if (option == "1")
            {
                WordExists();
            }
            else if (option == "2")
                Program.UserInput();
        }
        
        // Collect generic file info which are presented to user PrompDracula method
       static (string, long, int) FileInfo()
        {
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            string path = @"resources/dracula.txt";
            FileInfo fileInfo = new FileInfo(path);
            string name = fileInfo.Name;
            long size = fileInfo.Length;
            int lines = File.ReadAllLines(path).Length;

            string logString = $"File Name: {name}\nFile Size: {size}\nNumber of Lines: {lines}.";
            watch.Stop();
            long time = watch.ElapsedMilliseconds;
            Logging.WriteToFile(logString, time);
            
            return (name, size, lines);

        }

        static void WordExists ()
        {
            //Start watch to time function
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            //Provide user instructions and store user input
            Console.WriteLine("Type in word to you want to check: ...");
            string input = $" {Console.ReadLine()} ";
            string path = @"resources/dracula.txt";

            // Check for word and count number of times it occurs
            string text = File.ReadAllText(path);
            var count = Regex.Matches(text, $"(?i){input}").Count;
            if (count > 0)
            {
                string print = ($"The word {input} DOES exist and occurs {count} times");
                Console.WriteLine(print);
                watch.Stop();
                long time = watch.ElapsedMilliseconds;
                Logging.WriteToFile(print, time);
            }
            else
            {
                string print = ($"There is NO match of the word {input}");
                Console.WriteLine(print);
                watch.Stop();
                long time = watch.ElapsedMilliseconds;
                Logging.WriteToFile(print, time);
            }
     
            // Provide user feedback and navigation options
            Console.WriteLine("1. Search for different word");
            Console.WriteLine("2. Go back");
            string option = Console.ReadLine();
            if (option == "1")
            {
                WordExists();
            }
            else
            {
                PromptDracula();
            }

            
        }
    }
}
